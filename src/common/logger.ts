import { performance } from "perf_hooks";
import { LogLevel, IConfigService } from "./config";
import { isDevEnv } from "./util";

function getLogLevel(config: IConfigService): LogLevel {
    const logLevelKey = config.get("logLevel");
    if (logLevelKey != null) {
        const logLevel = LogLevel[logLevelKey];
        if (logLevel != null) {
            return logLevel;
        }
    }
    if (isDevEnv()) {
        return LogLevel.debug;
    } else {
        return LogLevel.error;
    }
}

export abstract class Logger {
    constructor(config: IConfigService) {
        this.logLevel = getLogLevel(config);
    }
    private readonly logLevel: LogLevel;
    private readonly _times: { [label: string]: number } = {};
    protected abstract log(level: string, x: unknown): void;
    public error(x: unknown): void {
        if (this.logLevel > LogLevel.error) {
            return;
        }
        this.log("[ERROR]", x);
    }

    public debug(x: unknown): void {
        if (this.logLevel > LogLevel.debug) {
            return;
        }
        this.log("[DEBUG]", x);
    }

    public timeStart(label: string): void {
        if (this.logLevel > LogLevel.debug) {
            return;
        }
        this._times[label] = performance.now();
    }
    public timeEnd(label: string): void {
        if (this.logLevel > LogLevel.debug) {
            return;
        }
        const t0 = this._times[label];
        if (t0) {
            const t1 = performance.now();
            delete this._times[label];
            const ms = (t1 - t0).toFixed(2);
            this.debug(`${label} ${ms} ms`);
        }
    }
}