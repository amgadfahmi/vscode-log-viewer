import * as assert from "assert";
import * as vscode from "vscode";
import { delay } from "../src/common/util";
import * as fs from "fs";
import * as path from "path";
import * as util from "util";

const exists = util.promisify(fs.exists);
const mkdir = util.promisify(fs.mkdir);
const unlink = util.promisify(fs.unlink);
const rmdir = util.promisify(fs.rmdir);
const stat = util.promisify(fs.stat);
const readdir = util.promisify(fs.readdir);
const appendFile = util.promisify(fs.appendFile);

function assertTruthy(x: any): asserts x {
    assert(x);
}

function randomString() {
    return Math.random().toString(36).substr(2);
}

interface FilesAndDirectories {
    files: string[];
    directories: string[];
}

async function createSomeFilesAndDirectories(rootDir: string): Promise<FilesAndDirectories> {

    const step = async (dir: string, num: number, accDirs: string[], accFiles: string[]) => {
        // folders
        for (let i = 0; i < num; i++) {
            const subDir = path.join(dir, randomString());
            await mkdir(subDir);
            accDirs.push(subDir);
            void step(subDir, num - 1, accDirs, accFiles);
        }

        // one file per level
        const file = path.join(dir, randomString() + ".log");
        accFiles.push(file);
    };
    const files: string[] = [];
    const directories: string[] = [];
    await step(rootDir, 3, directories, files);
    return {
        files,
        directories,
    };
}

async function deleteContents(dir: string): Promise<void> {
    for (const name of await readdir(dir)) {
        const x = path.join(dir, name);
        const s = await stat(x);
        if (s.isDirectory()) {
            await deleteContents(x);
            await rmdir(x);
        } else {
            await unlink(x);
        }
    }
}


suite("E2E", () => {
    test("Smoke test", async function () {
        const workspaceRoot = vscode.workspace.workspaceFolders?.[0];
        assertTruthy(workspaceRoot);
        const logDir = path.resolve(workspaceRoot.uri.fsPath, "log");
        if (await exists(logDir)) {
            await deleteContents(logDir);
        } else {
            await mkdir(logDir);
        }

        // makes logExplorer set __logViewer$logExplorerAndTreeView
        global.__logViewer$testing = true;

        const extension = vscode.extensions.getExtension("berublan.vscode-log-viewer");
        assertTruthy(extension);
        await extension.activate();

        const { logExplorer, treeView } = global.__logViewer$logExplorerAndTreeView;

        const first = logExplorer.getChildren()?.[0];
        assertTruthy(first);
        await treeView.reveal(first, { select: true, expand: true });

        const command = logExplorer.getTreeItem(first).command;
        assertTruthy(command);
        await vscode.commands.executeCommand(command.command, command.arguments);

        const textEditor = vscode.window.activeTextEditor;
        assertTruthy(textEditor);
        assert.equal(textEditor.document.getText(), "no matching file found");

        const filesAndDirs = await createSomeFilesAndDirectories(logDir);

        let waitIntervalMs: number;
        if (process.env.CI) {
            waitIntervalMs = 1000;
            this.timeout(20000);
        } else {
            waitIntervalMs = 200;
            this.timeout(5000);
        }

        for (let i = 0; i < 10; i++) {
            const someFile = filesAndDirs.files[Math.floor(Math.random() * filesAndDirs.files.length)];
            const lastAppended = randomString() + "\n";
            await appendFile(someFile, lastAppended);
            await delay(waitIntervalMs);
            const documentText = textEditor.document.getText();
            assert(documentText.endsWith(lastAppended), `"${documentText}" didn't end with "${lastAppended}"`);
        }

        await deleteContents(logDir);
        await delay(waitIntervalMs);

        assert.equal(textEditor.document.getText(), "no matching file found");
    });
});