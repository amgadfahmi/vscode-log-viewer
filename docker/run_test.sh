#!/bin/bash
set -euxo pipefail

# this export is required
export DISPLAY=:99.0

if [ -z "$(pidof /usr/bin/Xvfb)" ]
then
    Xvfb -ac $DISPLAY &
fi


cd /mnt/src/

yarn install
yarn run build
yarn run test
